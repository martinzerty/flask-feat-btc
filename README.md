# Flask-feat-BTC :v:
# Appli web Flask pour prix Bitcoin

Cette application web est faite avec le framework Flask pour python.
Elle utilise une API pour prendre le prix du bitcoin et l'affiche sur un template.

Rien de vraiment transendent mais c'était drôle d'expérimenter ça.

## Modules
```python
import requests
```
et 
```python
import flask
```

## API
[API blockchain](https://blockchain.info/ticker?base=BTC)